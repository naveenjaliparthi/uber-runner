#! /bin/bash

docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $REGISTRY_NAME
. image_info
. .version_info
echo "In push job args are: IMAGE_NAME: $IMAGE_NAME IMAGE_TAG: $IMAGE_TAG IMAGE_TAG_SHA: $IMAGE_TAG_SHA"
docker push "$IMAGE_FULL_PATH:$IMAGE_TAG"
docker push "$IMAGE_FULL_PATH:$IMAGE_TAG_SHA"
# for pre release versions 0.x we are making a 1.0.0-alpha image
if  [[ $IMAGE_TAG == 0.* ]]; then
    echo "pushing additional tag 1.0.0-alpha"
    docker tag "$IMAGE_FULL_PATH:$IMAGE_TAG_SHA" "$IMAGE_FULL_PATH:1.0.0-alpha"
    docker push "$IMAGE_FULL_PATH:1.0.0-alpha"
fi