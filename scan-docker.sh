#! /bin/bash

. image_info
. .version_info
echo "Scanning Image: $IMAGE_FULL_PATH:$IMAGE_TAG"
grype $IMAGE_FULL_PATH:$IMAGE_TAG > /tmp/grype

echo "Critical Vulnerabilities"
echo "-----------------------------------------------------------------------------------------------------"
cat /tmp/grype | grep Critical

echo "    "
echo "    "
echo "High Vulnerabilities"
echo "-----------------------------------------------------------------------------------------------------"
cat /tmp/grype | grep High

echo "    "
echo "    "
echo "Medium Vulnerabilities"
echo "-----------------------------------------------------------------------------------------------------"
cat /tmp/grype | grep Medium

echo "    "
echo "    "
echo "Low Vulnerabilities"
echo "-----------------------------------------------------------------------------------------------------"
cat /tmp/grype | grep Low

echo "    "
echo "    "
echo "Unknown Vulnerabilities"
echo "-----------------------------------------------------------------------------------------------------"
cat /tmp/grype | grep Unknown

echo "    "
echo "    "
echo "All Vulnerabilities"
echo "-----------------------------------------------------------------------------------------------------"
cat /tmp/grype
