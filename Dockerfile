#FROM maven:3.6.3-openjdk-8-slim
FROM registry.gitlab.com/juhani/go-semrel-gitlab:v0.21.1 AS semrel

FROM maven:3.6-openjdk-11-slim



COPY --from=semrel /usr/bin/release /usr/bin/release
RUN apt-get update && apt-get install -y -q git openssh-client wget
RUN mkdir /usr/bin/scripts & export PATH=$PATH:/usr/bin/scripts

# Add script
COPY *.sh /usr/bin/scripts/
COPY target/*.jar /
RUN chmod a+x /usr/bin/scripts/*.sh
RUN curl -fsSL https://download.docker.com/linux/static/stable/x86_64/docker-20.10.6.tgz -o /tmp/docker.tgz && tar --strip-components=1 -xvzf /tmp/docker.tgz -C /usr/local/bin && rm /tmp/docker.tgz
RUN curl -sSfL https://raw.githubusercontent.com/anchore/grype/main/install.sh | sh -s -- -b /usr/bin
#install sbt
#SBT
FROM openjdk:8u232
ARG SBT_VERSION=1.5.5
RUN \
  mkdir /working/ && \
  cd /working/ && \
  curl -L -o sbt-$SBT_VERSION.deb https://repo.scala-sbt.org/scalasbt/debian/sbt-$SBT_VERSION.deb && \
  dpkg -i sbt-$SBT_VERSION.deb && \
  rm sbt-$SBT_VERSION.deb && \
  apt-get update && \
  apt-get install sbt && \
  cd && \
  rm -r /working/ && \
  sbt sbtVersion

WORKDIR /
#CMD /bin/bash


#RUN chmod a+x /*.sh && apt update && apt install git openssh-client -y -q 

CMD ["/bin/bash"]