package com.indigo.utils.maven;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class POMUpdater {

	private static final String FILENAME = "src/test/resources/pom.xml";
	// xslt for pretty print only, no special task
	private static final String FORMAT_XSLT = "src/test/resources/xslt/staff-format.xslt";

//	private static String ARTIFACT_TO_FIND = "idms-am-extensions";

	public static final String CMD_GET_ARTIFACT_VERSION = "getArtifactVersion";
	public static final String CMD_BUMP_REQUIRED = "bumpRequired";
	public static final String CMD_UPDATE_DEPENDENCY_VERSION = "bumpDepVersion";
	public static final String CMD_UPDATE_ARTIFACT_VERSION = "bumpArtVersion";
	public static final String DEP_BUMP_REQUIRED = "DEP_BUMP_REQUIRED";

	public static final String USAGE = "USAGE\njava -jar maven-utils.jar <command> <options>\n"
			+ "where command is one of: " + CMD_GET_ARTIFACT_VERSION + " | " + CMD_BUMP_REQUIRED + " | "
			+ CMD_UPDATE_DEPENDENCY_VERSION + " | " + CMD_UPDATE_ARTIFACT_VERSION + "\n" + "\t"
			+ CMD_GET_ARTIFACT_VERSION + " path-to-pom \n" + "\t" + CMD_BUMP_REQUIRED
			+ " path-to-pom dependency-name dependency-version \n" + "\t" + CMD_UPDATE_DEPENDENCY_VERSION
			+ " path-to-pom dependency-name dependency-version artifact-version\n" + "\t" + CMD_UPDATE_ARTIFACT_VERSION
			+ " path-to-pom artifact-version \n";

	public static void main(String[] args) {
		if (args.length < 2) {
			System.err.println(USAGE);
			System.exit(-1);
		}

		String cmd = args[0];
		String pom = args[1];
		String depName = null;
		String depVersion = null;
		String artifactVersion = null;
		if (cmd.equalsIgnoreCase(CMD_GET_ARTIFACT_VERSION)) {
			Document doc = getPOM(pom);
			String ver = getAtrifactVersion(doc);
			System.out.println(ver);
			System.exit(0);
		} else if (cmd.equalsIgnoreCase(CMD_BUMP_REQUIRED)) {
			if (args.length != 4) {
				System.err.println(USAGE);
				System.exit(-1);
			}
			depName = args[2];
			depVersion = args[3];
			Document doc = getPOM(pom);

			String ver = getDependencyVersion(depName, doc);
			if (ver.equalsIgnoreCase(depVersion)) {
				System.out.println("0");
			} else {
				System.out.println("1");
			}
			System.exit(0);
		} else if (cmd.equalsIgnoreCase(CMD_UPDATE_DEPENDENCY_VERSION)) {
			if (args.length != 5) {
				System.err.println(USAGE);
				System.exit(-1);
			}
			depName = args[2];
			depVersion = args[3];
			artifactVersion = args[4];
			Document doc = getPOM(pom);

			updateDependencyVersion(depName, depVersion, doc);
			updateAtrifactVersion(doc, artifactVersion);
			try {
				persist(doc, pom);
			} catch (TransformerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.exit(-1);
			}

			System.exit(0);

		} else if (cmd.equalsIgnoreCase(CMD_UPDATE_ARTIFACT_VERSION)) {
			if (args.length != 3) {
				System.err.println(USAGE);
				System.exit(-1);
			}

			artifactVersion = args[2];
			Document doc = getPOM(pom);
			updateAtrifactVersion(doc, artifactVersion);
			try {
				persist(doc, pom);
			} catch (TransformerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.exit(-1);
			}

			System.exit(0);

		} else {
			System.err.println(USAGE);
			System.exit(-1);
		}

	}

	private static Document getPOM(String filename) {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;
		Document doc = null;
		InputStream is = null;
		try {
			db = dbf.newDocumentBuilder();
			is = new FileInputStream(filename);
			doc = db.parse(is);
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
			System.exit(-1);
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return doc;
	}

	private static String getAtrifactVersion(Document doc) {
		String version = "";
		Node proj = doc.getElementsByTagName("project").item(0);
		if (proj.getNodeType() == Node.ELEMENT_NODE) {
			Element el = (Element) proj;
			NodeList nodes = el.getElementsByTagName("version");
			for (int i = 0; i < nodes.getLength(); i++) {
				Node node = nodes.item(i);
				if (!node.getParentNode().getNodeName().equalsIgnoreCase("project")) {
					continue;
				}
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element element = (Element) node;
					version = element.getTextContent();
					break;
				}
			}
		}
		System.err.println("Atrifact Version is: " + version +"\n");
		return version;
	}
	private static void updateAtrifactVersion(Document doc, String version) {
		
		
		Node proj = doc.getElementsByTagName("project").item(0);
		if (proj.getNodeType() == Node.ELEMENT_NODE) {
			Element el = (Element) proj;
			NodeList nodes = el.getElementsByTagName("version");
			for (int i = 0; i < nodes.getLength(); i++) {
				Node node = nodes.item(i);
				if (!node.getParentNode().getNodeName().equalsIgnoreCase("project")) {
					continue;
				}
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element element = (Element) node;
				    element.setTextContent(version);
					break;
				}
			}
		}

		System.err.println("Updated Atrifact Verdsion to: " + version);

	}

	private static void updateAtrifactVersion2(Document doc, String version) {

		Node proj = doc.getElementsByTagName("project").item(0);
		if (proj.getNodeType() == Node.ELEMENT_NODE) {
			Element element = (Element) proj;
			Node node1 = element.getElementsByTagName("version").item(0);
			node1.setTextContent(version);
		}
		System.err.println("Atrifact Verdsion is: " + version);

	}

	private static String getDependencyVersion(String depName, Document doc) {

		String artifactId = null;
		String groupId = null;
		String version = null;

		NodeList deps = doc.getElementsByTagName("dependency");
		for (int i = 0; i < deps.getLength(); i++) {
			Node dep = deps.item(i);
			if (dep.getNodeType() == Node.ELEMENT_NODE) {
				Element element = (Element) dep;
				Node node1 = element.getElementsByTagName("artifactId").item(0);
				if (node1.getTextContent().equalsIgnoreCase(depName)) {
					groupId = element.getElementsByTagName("groupId").item(0).getTextContent();
					artifactId = element.getElementsByTagName("artifactId").item(0).getTextContent();
					version = element.getElementsByTagName("version").item(0).getTextContent();
					break;
				}
			}
		}

		System.err.println("Dependency: " + depName + " groupId:" + groupId + " artifactId:" + artifactId
				+ " version:" + version);
		return version;
	}

	private static void updateDependencyVersion(String depName, String depVersion, Document doc) {
		NodeList deps = doc.getElementsByTagName("dependency");
		for (int i = 0; i < deps.getLength(); i++) {
			Node dep = deps.item(i);
			if (dep.getNodeType() == Node.ELEMENT_NODE) {
				Element element = (Element) dep;
				Node node1 = element.getElementsByTagName("artifactId").item(0);
				if (node1.getTextContent().equalsIgnoreCase(depName)) {

					element.getElementsByTagName("version").item(0).setTextContent(depVersion);
					break;
				}
			}
		}

		// System.out.println("Dependency: " + ARTIFACT_TO_FIND + " groupId:" +
		// groupId + " artifactId:" + artifactId + " version:" + version);

	}

	private static void persist(Document document, String xmlFilePath) throws TransformerException {
		TransformerFactory transformerFactory = TransformerFactory.newInstance();

		Transformer transformer = transformerFactory.newTransformer();
		DOMSource domSource = new DOMSource(document);

		StreamResult streamResult = new StreamResult(new File(xmlFilePath));
		transformer.transform(domSource, streamResult);

		// System.err.println("The XML File was ");
	}

	// write doc to output stream
	private static void writeXml(Document doc, OutputStream output)
			throws TransformerException, UnsupportedEncodingException {

		TransformerFactory transformerFactory = TransformerFactory.newInstance();

		// The default add many empty new line, not sure why?
		// https://stackoverflow.com/questions/58478632/how-to-avoid-extra-blank-lines-in-xml-generation-with-java
		// https://mkyong.com/java/pretty-print-xml-with-java-dom-and-xslt/
		// Transformer transformer = transformerFactory.newTransformer();

		// add a xslt to remove the extra newlines
		Transformer transformer = transformerFactory.newTransformer(new StreamSource(new File(FORMAT_XSLT)));

		// pretty print
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty(OutputKeys.STANDALONE, "no");

		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(output);

		transformer.transform(source, result);

	}

}