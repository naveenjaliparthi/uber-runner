#! /bin/bash

eval $(ssh-agent -s)
ssh-add <(echo "$GL_SSH_PRIVATE_KEY")
git config --global user.name "${GITLAB_USER_NAME}"
git config --global user.email "${GITLAB_USER_EMAIL}"
mkdir -p ~/.ssh
chmod 700 ~/.ssh
#[[ -f /.dockerenv ]] && echo -e Host *\n\tStrictHostKeyChecking no\n\n > ~/.ssh/config
echo "$GL_SSH_KNOWN_HOSTS" >> ~/.ssh/known_hosts
chmod 644 ~/.ssh/known_hosts


cat .version_info
. .version_info
echo "next version is $RELEASE_VERSION"
echo "current artifact version is $ARTIFACT_VERSION"
echo "artifact DEP_BUMP $DEP_BUMP"
FINAL_ARTIFACT_VERSION=""
ART_BUMP=0


if [[ "$ARTIFACT_VERSION" != "$RELEASE_VERSION" ]]; then
  echo "ARTIFACT_VERSION=$ARTIFACT_VERSION and RELEASE_VERSION=$RELEASE_VERSION do not match "
  FINAL_ARTIFACT_VERSION=$RELEASE_VERSION
  ART_BUMP=1
elif [[ "$DEP_BUMP" == 1 ]]; then
  echo "Using FINAL_ARTIFACT_VERSION due to upstream bump $ARTIFACT_BUMP_VERSION"
  FINAL_ARTIFACT_VERSION=$ARTIFACT_BUMP_VERSION
fi


echo "checking is we need to update pom"
if [ "$DEP_BUMP" == 1 ] || [ "$ART_BUMP" == 1 ]; then
git remote rm origin && git remote add origin git@git.indigoconsulting.com:$CI_PROJECT_PATH.git
git pull origin HEAD:"$CI_COMMIT_BRANCH"
  if [ "$DEP_BUMP" == 1 ]; then
    echo "executing java -jar /maven-utils.jar bumpDepVersion pom.xml $UPSTREAM_DEPENDENCY_NAME $UPSTREAM_DEPENDENCY_VERSION $FINAL_ARTIFACT_VERSION"
    java -jar /maven-utils.jar bumpDepVersion pom.xml $UPSTREAM_DEPENDENCY_NAME $UPSTREAM_DEPENDENCY_VERSION $FINAL_ARTIFACT_VERSION 
  elif [[ "$ART_BUMP" == 1 ]]; then
    echo "executing java -jar /maven-utils.jar bumpArtVersion pom.xml $FINAL_ARTIFACT_VERSION"
    java -jar /maven-utils.jar bumpArtVersion pom.xml $FINAL_ARTIFACT_VERSION 
  fi
#    cat pom.xml
  git add pom.xml
  git commit -m "fix(upstream): Updating pom and or dependency $UPSTREAM_DEPENDENCY_NAME:$UPSTREAM_DEPENDENCY_VERSION  project version to $FINAL_ARTIFACT_VERSION"
  git push --push-option='ci.skip' origin HEAD:"$CI_COMMIT_BRANCH"
  #git push origin HEAD:"$CI_COMMIT_BRANCH"
fi


echo "RELEASE_URL=$CI_PROJECT_URL/-/releases" > build_info
echo "RELEASE_DESC=\"$(uname -mo) binary\"" >> build_info
echo "RELEASE_SHA=$CI_COMMIT_SHA" >> build_info
echo "RELEASE_VERSION=$RELEASE_VERSION" >> build_info
cat build_info

# only build if target exists, to take care of fabric 8
if [[ -z "${FABRIC8}" ]]; then
if [[ -f image_info ]] && [[ -d target ]]; then
  . image_info
  echo "In build job args are FROM_IMAGE_FULL_PATH $FROM_IMAGE_FULL_PATH IMAGE_NAME $IMAGE_NAME IMAGE_TAG $IMAGE_TAG IMAGE_TAG_SHA $IMAGE_TAG_SHA"
  if [[ ! -z "${FROM_IMAGE_FULL_PATH}" ]]; then
    docker build --no-cache=true --pull --build-arg FROM_IMAGE_FULL_PATH=$FROM_IMAGE_FULL_PATH --tag $IMAGE_FULL_PATH:$IMAGE_TAG --tag $IMAGE_FULL_PATH:$IMAGE_TAG_SHA target
  else
    docker build --no-cache=true --pull --tag $IMAGE_FULL_PATH:$IMAGE_TAG --tag $IMAGE_FULL_PATH:$IMAGE_TAG_SHA target
  fi
fi
fi
