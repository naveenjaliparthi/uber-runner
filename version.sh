#! /bin/bash

git pull origin HEAD:"$CI_COMMIT_BRANCH"
UPSTREAM_BUMP=0
UPSTREAM_BUMP_VERSION=""

DEP_BUMP=0
DEP_NAME=""
DEP_VERSION=""
ARTIFACT_BUMP_VERSION=""

if [[ -f image_info ]]; then
  . image_info
fi

touch .version_info
if [[ -z "${IMAGE_NAME}" ]]; then
  IMAGE_NAME=$CI_PROJECT_NAME
  echo "IMAGE_NAME=$CI_PROJECT_NAME" >> .version_info
  echo "NO IMAGE_NAME from iamge_info file setting IMAGE_NAME to project name $CI_PROJECT_NAME"
else
  echo "IMAGE_NAME=$IMAGE_NAME" >> .version_info
  echo "received IMAGE_NAME from image_info  $IMAGE_NAME"
fi



if [[ -f pom.xml ]]; then
  echo "pom.xml found"
  av=$(java -jar /maven-utils.jar getArtifactVersion pom.xml)
  echo "ARTIFACT_VERSION=$av"
  echo "ARTIFACT_VERSION=$av" >> .version_info
  
  if [[ ! -z "${UPSTREAM_DEPENDENCY_NAME}" ]]; then
    echo "upstream dep found $UPSTREAM_DEPENDENCY_NAME $UPSTREAM_DEPENDENCY_VERSION"
    echo "UPSTREAM_DEPENDENCY_NAME="$UPSTREAM_DEPENDENCY_NAME >> .version_info
    echo "UPSTREAM_DEPENDENCY_VERSION="$UPSTREAM_DEPENDENCY_VERSION >> .version_info    
    rb=$(java -jar /maven-utils.jar bumpRequired pom.xml $UPSTREAM_DEPENDENCY_NAME $UPSTREAM_DEPENDENCY_VERSION)
    DEP_BUMP=$rb
    echo "DEP_BUMP=$DEP_BUMP" >> .version_info
    ARTIFACT_BUMP_VERSION=$(release next-version --bump-patch)
    echo "ARTIFACT_BUMP_VERSION=$ARTIFACT_BUMP_VERSION" >> .version_info
  else
    echo "no upstream dep bump required"
    echo "DEP_BUMP=0" >> .version_info
  fi
else
  echo "No pom.xml and no UPSTREAM_DEPENDENCY_NAME found "
fi


if [[ ! -z "${UPSTREAM_IMAGE_TAG}" ]]; then
  if [[ -f image_info ]]; then
    . image_info
    echo "image_info found and upstream tag found $UPSTREAM_IMAGE_TAG"
    old_base_ver=$BASE_IAMGE_TAG
    echo "comparing old_base_ver $old_base_ver new_base_ver $UPSTREAM_IMAGE_TAG"
    if [[ "$old_base_ver" == "$UPSTREAM_IMAGE_TAG" ]]; then
      echo "base versions match no upstream bump required"
    else
      echo "base versions DO NOT match upstream bump required, updating base-image-tag to $UPSTREAM_IMAGE_TAG"
#     sed -ir "s/^[#]*\s*BASE_IAMGE_TAG=.*/BASE_IAMGE_TAG=$UPSTREAM_IMAGE_TAG/" image_info
      UPSTREAM_BUMP=1
      # note for upstream image change we bump the patch version
      UPSTREAM_BUMP_VERSION=$(release next-version --bump-patch)
    fi
  else
    echo "No image_info file found no upstream bump required"
  fi
else
  echo "No upstream image tag found no upstream bump required"
fi


echo "UPSTREAM_BUMP=$UPSTREAM_BUMP" >> .version_info
echo "UPSTREAM_BUMP_VERSION=$UPSTREAM_BUMP_VERSION" >> .version_info

NEED_LOCAL_RELEASE=0
new_ver=$(release next-version --allow-current)
if [[ -f release_info ]]; then
  echo "release_info found"
  old_ver=`grep RELEASE_VERSION release_info |cut -d '=' -f 2`
  echo "comparing old_ver $old_ver new_ver $new_ver"
  if [[ ! "$old_ver" == "$new_ver" ]]; then
    echo "version DO NOT match release required"
    NEED_LOCAL_RELEASE=1
  fi
else
  NEED_LOCAL_RELEASE=1
fi

echo "NEED_RELEASE=$NEED_LOCAL_RELEASE" >> .version_info

if [ "${NEED_LOCAL_RELEASE}" == 1 ]; then
  echo -n "update release version to $new_ver "
  RELEASE_VERSION=$new_ver
elif [ "${UPSTREAM_BUMP}" == 1 ]; then
  echo "bump required for base image update release version to $UPSTREAM_BUMP_VERSION"
  RELEASE_VERSION=$UPSTREAM_BUMP_VERSION
elif [ "${DEP_BUMP}" == 1 ]; then
  echo "bump required for maven dependency update release version to $ARTIFACT_BUMP_VERSION"
  RELEASE_VERSION=$ARTIFACT_BUMP_VERSION
else
  echo -n "no update or bump setting release version to $new_ver "
  RELEASE_VERSION=$new_ver
fi
echo "RELEASE_VERSION=$RELEASE_VERSION" >> .version_info
echo "IMAGE_FULL_PATH=${REGISTRY_NAME}${REGISTRY_DOCKER_BASE}${IMAGE_NAME}" >> .version_info



if [ ! $CI_COMMIT_REF_NAME == 'master' ]; then
  echo "IMAGE_TAG=$RELEASE_VERSION-$CI_COMMIT_REF_SLUG" >> .version_info
  IMAGE_TAG=$RELEASE_VERSION-$CI_COMMIT_REF_SLUG
else
  echo "IMAGE_TAG=$RELEASE_VERSION" >> .version_info
  IMAGE_TAG=$RELEASE_VERSION
fi

echo "IMAGE_TAG_SHA=$IMAGE_TAG-$CI_COMMIT_SHORT_SHA" >> .version_info

if [[ -z "${FROM_IMAGE_FULL_PATH}" ]]; then
  FROM_IMAGE_FULL_PATH=${BASE_IMAGE_NAME}:${BASE_IAMGE_TAG}
  echo "FROM_IMAGE_FULL_PATH=${BASE_IMAGE_NAME}:${BASE_IAMGE_TAG}" >> .version_info
  echo "didn't received FROM_IMAGE_FULL_PATH and TAG from image_info setting FROM_IMAGE_FULL_PATH to ${FROM_IMAGE_FULL_PATH}"
else
  FROM_IMAGE_FULL_PATH=${BASE_IMAGE_NAME}:${UPSTREAM_IMAGE_TAG}
  echo "FROM_IMAGE_FULL_PATH=${BASE_IMAGE_NAME}:${UPSTREAM_IMAGE_TAG}" >> .version_info
  echo "received BASE_IMAGE_NAME and TAG from image_info setting FROM_IMAGE_FULL_PATH to ${FROM_IMAGE_FULL_PATH}"
fi


cat .version_info
