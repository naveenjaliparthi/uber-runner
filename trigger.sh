#! /bin/bash

. .version_info
echo "In trigger project_id $PROJECT_ID"

touch .version_info
if [[ -z "${UPSTREAM_DEPENDENCY_NAME}" ]]; then
  UPSTREAM_DEPENDENCY_NAME=$CI_PROJECT_NAME
  echo "no UPSTREAM_DEPENDENCY_NAME specified using project name $UPSTREAM_DEPENDENCY_NAME"
elif [[ "${UPSTREAM_DEPENDENCY_NAME}" != "${CI_PROJECT_NAME}" ]]; then
  echo "OOPS!! UPSTREAM_DEPENDENCY_NAME $UPSTREAM_DEPENDENCY_NAME and project name $CI_PROJECT_NAME DO NOT match using $UPSTREAM_DEPENDENCY_NAME"
  UPSTREAM_DEPENDENCY_NAME=$CI_PROJECT_NAME
fi

if [ "$NEED_RELEASE" == 1 ] || [ "$UPSTREAM_BUMP" == 1 ] || [ "$DEP_BUMP" == 1 ]; then
  # we need to sleep so that this trigger can complete and the version info is not overridden by another job
  SLEEP=${SLEEP_SECS:-20}
  echo "sleeping for $SLEEP secs"
  sleep $SLEEP
    
    
  if [[ -f pom.xml ]]; then
    echo "triggering downstream project $PROJECT_ID with $UPSTREAM_DEPENDENCY_NAME:$RELEASE_VERSION for maven dependency"
    wget -qO- \
    --post-data "token=$CI_JOB_TOKEN&ref=master&variables[UPSTREAM_DEPENDENCY_NAME]=$UPSTREAM_DEPENDENCY_NAME&variables[UPSTREAM_DEPENDENCY_VERSION]=$RELEASE_VERSION" \
"https://git.indigoconsulting.com/api/v4/projects/$PROJECT_ID/trigger/pipeline"
    echo "triggered downstream"
    
  else
    echo "triggering downstream $PROJECT_ID with UPSTREAM_IMAGE_TAG $IMAGE_TAG $UPSTREAM_DEPENDENCY_NAME:$RELEASE_VERSION for base image dependency"
    wget -qO- \
    --post-data "token=$CI_JOB_TOKEN&ref=master&variables[UPSTREAM_IMAGE_TAG]=$IMAGE_TAG&variables[FROM_IMAGE_FULL_PATH=${BASE_IMAGE_NAME}:${IMAGE_TAG}]" \
"https://git.indigoconsulting.com/api/v4/projects/$PROJECT_ID/trigger/pipeline"
    echo "triggered downstream"
    
  fi    
        
else
   echo "no release or upstream bump required NOT invoking trigger"
fi
