#! /bin/bash

eval $(ssh-agent -s)
ssh-add <(echo "$GL_SSH_PRIVATE_KEY")
git config --global user.name "${GITLAB_USER_NAME}"
git config --global user.email "${GITLAB_USER_EMAIL}"
mkdir -p ~/.ssh
chmod 700 ~/.ssh
#[[ -f /.dockerenv ]] && echo -e �Host *\n\tStrictHostKeyChecking no\n\n� > ~/.ssh/config
echo "$GL_SSH_KNOWN_HOSTS" >> ~/.ssh/known_hosts
chmod 644 ~/.ssh/known_hosts


. .version_info


echo "Need UPSTREAM_BUMP $UPSTREAM_BUMP"
echo "Need Release $NEED_RELEASE"



if [[ "$UPSTREAM_BUMP" == 1 ]]; then
  echo "pulling latest changes from branch $CI_COMMIT_BRANCH"
  git pull origin HEAD:"$CI_COMMIT_BRANCH"
  sed -ir "s/^[#]*\s*BASE_IAMGE_TAG=.*/BASE_IAMGE_TAG=$UPSTREAM_IMAGE_TAG/" image_info
  cat image_info
  git remote rm origin && git remote add origin git@git.indigoconsulting.com:$CI_PROJECT_PATH.git
  git add image_info
  git commit -m "fix(upstream): Updating base image tag to $UPSTREAM_IMAGE_TAG"
  git push --push-option='ci.skip' origin HEAD:"$CI_COMMIT_BRANCH"
fi




if [ "$NEED_RELEASE" == 1 ] || [ "$UPSTREAM_BUMP" == 1 ] || [ "$DEP_BUMP" == 1 ]; then
  echo "pulling latest changes from branch $CI_COMMIT_BRANCH"
  git pull origin HEAD:"$CI_COMMIT_BRANCH"
  #git checkout "$CI_COMMIT_BRANCH"
  rm -f release_info
  mv build_info release_info
  echo "RELEASE_DATE=`date +%m-%d-%Y:%H-%M-%S:%Z`" >> release_info
  . release_info
  echo -n "Releasing version  v$RELEASE_VERSION ..."
  release changelog
  release commit-and-tag image_info CHANGELOG.md release_info
  release --ci-commit-tag v$RELEASE_VERSION add-download-link --name release --url $RELEASE_URL --description "$RELEASE_DESC"
else
  echo "no changes skipping release"
fi
